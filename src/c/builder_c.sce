// ====================================================================
// Allan CORNET
// DIGITEO
// ====================================================================
function builder_c()

tbx_build_src('dde_c', 'dde.c', 'c', get_absolute_file_path('builder_c.sce'));

endfunction

builder_c();
clear builder_c; // remove builder_c on stack
// ====================================================================

