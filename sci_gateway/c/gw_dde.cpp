#include <wchar.h>
#include "gw_dde.hxx"
extern "C"
{
#include "gw_dde.h"
#include "addfunction.h"
}

#define MODULE_NAME L"gw_dde"

int gw_dde(wchar_t* _pwstFuncName)
{
    if(wcscmp(_pwstFuncName, L"ddeclose") == 0){ addCFunction(L"ddeclose", &sci_ddeclose, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"ddeexec") == 0){ addCFunction(L"ddeexec", &sci_ddeexec, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"ddeisopen") == 0){ addCFunction(L"ddeisopen", &sci_ddeisopen, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"ddepoke") == 0){ addCFunction(L"ddepoke", &sci_ddepoke, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"ddereq") == 0){ addCFunction(L"ddereq", &sci_ddereq, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"ddeopen") == 0){ addCFunction(L"ddeopen", &sci_ddeopen, MODULE_NAME); }

    return 1;
}
