// ====================================================================
// Allan CORNET
// Copyright DIGITEO 2008 - 2011
// ====================================================================
function build_main_help()
  help_dir = get_absolute_file_path('builder_help.sce');
  tbx_builder_help_lang("en_US", help_dir);
endfunction
// ====================================================================
build_main_help();
clear build_main_help;
// ====================================================================

